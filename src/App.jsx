import { ThemeProvider } from '@mui/material/styles';
import { createBrowserHistory } from 'history';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { BrowserRouter } from 'react-router-dom';
import BaseLayout from './components/layout/BaseLayout';
import { ErrorProvider } from './contexts/ErrorContext';
import theme from './theme';

const browserHistory = createBrowserHistory();

const App = () => {
	const { i18n } = useTranslation();

	useEffect(() => {
		i18n.changeLanguage(localStorage.getItem('language'));
	}, [i18n]);

	return (
		<ThemeProvider theme={theme}>
			<ErrorProvider>
				<BrowserRouter history={browserHistory}>
					<BaseLayout />
				</BrowserRouter>
			</ErrorProvider>
		</ThemeProvider>
	);
};

export default App;
