import LogIn from '../pages/Login/LogIn';
import Register from '../pages/Register/Register';

const routes = [
	{
		path: '/',
		exact: true,
		component: LogIn,
		name: 'LogIn',
		protected: false,
	},
	{
		path: '/register',
		exact: true,
		component: Register,
		name: 'Register',
		protected: false,
	},
];

export default routes;
