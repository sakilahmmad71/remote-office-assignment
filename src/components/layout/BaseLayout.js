/* eslint-disable no-self-compare */
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Suspense } from 'react';
import routeConfig from '../../routes';
import routes from '../../routes/routes';
import ProgressBar from '../atoms/ProgressBar';
import { FOOTER_HEIGHT, HEADER_HEIGHT } from './constants';

const useStyles = makeStyles((theme) => ({
	root: {
		minHeight: `calc(100vh - ${HEADER_HEIGHT + FOOTER_HEIGHT}px) !important`,
		background: theme.palette.primary.sec,
		[theme.breakpoints.down('md')]: {
			minHeight: `100vh`,
		},
	},
}));

const BaseLayout = () => {
	const classes = useStyles();

	return (
		<>
			{/* <Header /> */}
			<Suspense fallback={<ProgressBar />}>
				<Box className={classes.root}>{routeConfig(routes)}</Box>
			</Suspense>
			{/* <Footer /> */}
		</>
	);
};

export default BaseLayout;
