/* eslint-disable prettier/prettier */
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import {
	Box,
	Button,
	Checkbox,
	Divider,
	FormControlLabel,
	FormGroup,
	Grid,
	List,
	ListItem,
	ListItemIcon,
	Paper,
	Typography,
	useMediaQuery,
	useTheme
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Form, Formik } from 'formik';
import React from 'react';
import { Link } from 'react-router-dom';
import GoogleIcon from '../../assets/google.svg';
import BrandLogo from '../../assets/logo.svg';
import PasswordInputField from '../../components/modecules/PasswordInputField';
import TextInputField from '../../components/modecules/TextInputField';

const useStyles = makeStyles((theme) => ({
	container: {
		textAlign: 'center',
		color: theme.palette.text.secondary,
		minHeight: `100vh !important`,
	},
}));

const featureItems = [
	{ id: 'fd5d196c-d15a-11ec-9d64-0242ac120002', feature: 'Upload documents' },
	{ id: 'fc1ea31b2-dcf0-472c-8bcc-e0813a5ce6ed', feature: 'Update communications preferences' },
	{ id: 'f67bc143-444f-4335-8cba-8ea08828de78', feature: 'Managing billing and invoices' },
];

const Register = () => {
	const theme = useTheme();
	const isSmallDevice = useMediaQuery(theme.breakpoints.down('md'));
	const classes = useStyles();

	const initialValues = {
		email: '',
		password: '',
		terms: false,
	};

	const handleSubmitSignin = async (data, fn) => console.log({ data, fn });

	return (
		<Grid container className={classes.container}>
			<Grid
				item
				xl={5}
				lg={5}
				md={5}
				sx={{
					background: 'linear-gradient(127.55deg, #141E30 3.73%, #243B55 92.26%)',
					display: { xs: 'none', sm: 'none', md: 'block' },
				}}
			>
				<Grid
					item
					xl={10}
					lg={10}
					md={10}
					sx={{
						margin: '0 auto',
						minHeight: '100%',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Box sx={{ color: '#FFFFFF', display: 'flex', flexDirection: 'column', gap: 5 }}>
						<Typography variant="h4" fontWeight="bolder">
							Our Leg Up
						</Typography>

						<Box>
							<img
								src={BrandLogo}
								alt="Google"
								height={133}
								width={133}
								style={{
									border: '10px #fffefe33 solid',
									borderRadius: '50%',
								}}
							/>
							<Typography variant="h6">Equity Investor Dashboard</Typography>
						</Box>

						<Typography variant="body1" fontWeight="normal">
							This dashboard is for Equity pledge customers of Our Leg Up to manage their
							onboarding, payouts, communications, support, account details and retrieve invoices.
						</Typography>

						<Divider sx={{ background: '#fffefe33' }} />

						<Box textAlign="left">
							<Typography variant="h5" fontWeight="bold">
								Sign-in to your dashboard
							</Typography>

							<List>
								{featureItems?.map((item) => (
									<ListItem key={item.id}>
										<ListItemIcon>
											<CheckCircleOutlineIcon sx={{ color: '#44D600' }} />
										</ListItemIcon>
										<Typography fontWeight="bold" sx={{ marginLeft: -2.5 }}>
											{item.feature}
										</Typography>
									</ListItem>
								))}
							</List>
						</Box>
					</Box>
				</Grid>
			</Grid>

			<Grid item md={7} sm={12} xs={12} py={3}>
				<Grid
					item
					md={10}
					sm={12}
					xs={12}
					sx={{
						margin: '0 auto',
						minHeight: '100%',
						minWidth: '100%',
						display: 'flex',
						flexDirection: 'column',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
				{isSmallDevice && <Box sx={{ display: { xs: 'none', sm: 'block' }, marginBottom: { xs: 0, sm: 3 } }}>
					<img src={BrandLogo} alt="Google" height={70} width={70} />
				</Box>}
				
					<Box
						component={isSmallDevice ? Paper : Box}
						elevation={3}
						sx={{
							minWidth: '70%',
							padding: '30px',
							display: 'flex',
							flexDirection: 'column',
							gap: 3,
						}}
					>
						<Box
							textAlign={isSmallDevice ? "left" : "center"}
							sx={{ display: { xs: 'flex', sm: 'block' }, justifyContent: { xs: 'space-between' } }}
						>
							<Box>
								<Typography variant="h4">Sign up</Typography>
								<Typography variant="caption">Fill in the fields below to create your account.</Typography>
							</Box>

							<Box sx={{ display: { xs: 'block', sm: 'none' } }}>
								<img src={BrandLogo} alt="Google" height={70} width={70} />
							</Box>
						</Box>

						<Button type="submit" variant="outlined" fullWidth sx={{ textTransform: 'none' }}>
							<Box
								sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 0.5 }}
							>
								<img src={GoogleIcon} alt="Google" height={20} width={20} />
								<Typography>Sign up with Google</Typography>
							</Box>
						</Button>

						<Divider>
							<Typography>OR</Typography>
						</Divider>

						<Formik initialValues={initialValues} onSubmit={handleSubmitSignin}>
							{() => (
								<Form style={{ display: 'flex', flexDirection: 'column', gap: '17px' }}>
									<TextInputField
										fullWidth
										label="Email address"
										name="email"
										type="email"
										placeholder="example@email.com"
									/>
									<PasswordInputField
										fullWidth
										label="Password"
										name="password"
										placeholder="********"
									/>

									<FormGroup>
										<FormControlLabel
											control={<Checkbox name="terms" />}
											name="terms"
											label={
												<span>
													I accept the{' '}
													<Link to="/" style={{ textDecoration: 'none' }}>
														<Typography component="span" color="primary">
															terms and conditions
														</Typography>
													</Link>
												</span>
											}
										/>
									</FormGroup>

									<Button type="submit" variant="contained" fullWidth sx={{ textTransform: 'none' }}>
										Sign up
									</Button>
								</Form>
							)}
						</Formik>

						<Typography textAlign="left" pt={1}>
						Already have an account?{' '}
							<Link to="/" style={{ textDecoration: 'none' }}>
								<Typography component="span" color="primary">
								Sign in here
								</Typography>
							</Link>
						</Typography>
					</Box>
				</Grid>
			</Grid>
		</Grid>
	);
};

export default Register;
