import { Box, Button, Checkbox, Divider, FormControlLabel, FormGroup, Grid, Paper, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Form, Formik } from 'formik';
import { Link } from 'react-router-dom';
import GoogleIcon from '../../assets/google.svg';
import BrandLogo from '../../assets/logo.svg';
import PasswordInputField from '../../components/modecules/PasswordInputField';
import TextInputField from '../../components/modecules/TextInputField';

const useStyles = makeStyles((theme) => ({
	container: {
		textAlign: 'center',
		color: theme.palette.text.secondary,
		minHeight: `100vh !important`,
		justifyContent: 'center',
		alignItems: 'center',
	},
}));

const LogIn = () => {
	const classes = useStyles();

	const initialValues = {
		email: '',
		password: '',
		terms: false,
	};

	const handleSubmitSignin = async (data, fn) => console.log({ data, fn });

	return (
		<Grid container className={classes.container}>
			<Grid item xl={4} lg={4} md={4} sm={10} xs={12} py={3}>
				<Box sx={{ display: { xs: 'none', sm: 'block' }, marginBottom: { xs: 0, sm: 3 } }}>
					<img src={BrandLogo} alt="Google" height={70} width={70} />
				</Box>

				<Paper elevation={3} sx={{ padding: '30px', display: 'flex', flexDirection: 'column', gap: 3 }}>
					<Box textAlign="left" sx={{ display: { xs: 'flex', sm: 'block' }, justifyContent: { xs: 'space-between' } }}>
						<Box>
							<Typography variant="h4">Sign in</Typography>
							<Typography variant="caption">Fill in the fields below to sign into your account. </Typography>
						</Box>

						<Box sx={{ display: { xs: 'block', sm: 'none' } }}>
							<img src={BrandLogo} alt="Google" height={70} width={70} />
						</Box>
					</Box>

					<Button type="submit" variant="outlined" fullWidth sx={{ textTransform: 'none' }}>
						<Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 0.5 }}>
							<img src={GoogleIcon} alt="Google" height={20} width={20} />
							<Typography>Sign in with Google</Typography>
						</Box>
					</Button>

					<Divider>
						<Typography>OR</Typography>
					</Divider>

					<Formik initialValues={initialValues} onSubmit={handleSubmitSignin}>
						{() => (
							<Form style={{ display: 'flex', flexDirection: 'column', gap: '17px' }}>
								<TextInputField
									fullWidth
									label="Email address"
									name="email"
									type="email"
									placeholder="example@email.com"
								/>
								<PasswordInputField fullWidth label="Password" name="password" placeholder="********" />

								<FormGroup>
									<FormControlLabel
										control={<Checkbox name="terms" />}
										name="terms"
										label={
											<span>
												I accept the{' '}
												<Link to="/" style={{ textDecoration: 'none' }}>
													<Typography component="span" color="primary">
														terms and conditions
													</Typography>
												</Link>
											</span>
										}
									/>
								</FormGroup>

								<Button type="submit" variant="contained" fullWidth sx={{ textTransform: 'none' }}>
									Sign in with your email
								</Button>
							</Form>
						)}
					</Formik>

					<Typography textAlign="left" pt={1}>
						Don’t have an account, yet?{' '}
						<Link to="/register" style={{ textDecoration: 'none' }}>
							<Typography component="span" color="primary">
								Sign up here
							</Typography>
						</Link>
					</Typography>
				</Paper>
			</Grid>
		</Grid>
	);
};

export default LogIn;
